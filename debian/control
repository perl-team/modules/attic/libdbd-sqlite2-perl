Source: libdbd-sqlite2-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Niko Tyni <ntyni@iki.fi>,
           Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Ansgar Burchardt <ansgar@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-perl-dbi,
               libdbi-perl,
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbd-sqlite2-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbd-sqlite2-perl.git
Homepage: https://metacpan.org/release/DBD-SQLite2
Rules-Requires-Root: no

Package: libdbd-sqlite2-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libdbi-perl
Description: Perl DBI driver with a self-contained RDBMS (SQLite2 version)
 The DBD::SQLite2 module embeds a small fast embedded SQL database engine
 called SQLite into a DBI driver, if you want a relational database for your
 project, but don't want to install a large RDBMS system like MySQL or
 PostgreSQL, then DBD::SQLite may be just what you need.
 .
 SQLite supports the following features:
 .
  * Implements a large subset of SQL92
    See http://www.hwaci.com/sw/sqlite/lang.html for details.
 .
  * A complete DB in a single disk file
    Everything for your database is stored in a single disk file, making it
    easier to move things around than with DBD::CSV.
 .
  * Atomic commit and rollback
 .
 The engine is very fast, but for updates/inserts/dml it does perform
 a global lock on the entire database.  This, obviously, might not be
 good for multiple user systems.  So beware.  The database also
 appears to be significantly faster if your transactions are coarse.
